/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sesiones;

import Entidades.Detalleasunto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marii
 */
@Stateless
public class DetalleasuntoFacade extends AbstractFacade<Detalleasunto> {
    @PersistenceContext(unitName = "VotacionesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleasuntoFacade() {
        super(Detalleasunto.class);
    }
    
}
