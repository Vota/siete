/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sesiones;

import Entidades.Asuntovotacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Marii
 */
@Stateless
public class AsuntovotacionFacade extends AbstractFacade<Asuntovotacion> {
    @PersistenceContext(unitName = "VotacionesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AsuntovotacionFacade() {
        super(Asuntovotacion.class);
    }
    
}
