/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marii
 */
@Entity
@Table(name = "detalleasunto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalleasunto.findAll", query = "SELECT d FROM Detalleasunto d"),
    @NamedQuery(name = "Detalleasunto.findById", query = "SELECT d FROM Detalleasunto d WHERE d.id = :id"),
    @NamedQuery(name = "Detalleasunto.findByGanancia", query = "SELECT d FROM Detalleasunto d WHERE d.ganancia = :ganancia"),
    @NamedQuery(name = "Detalleasunto.findByTiempo", query = "SELECT d FROM Detalleasunto d WHERE d.tiempo = :tiempo"),
    @NamedQuery(name = "Detalleasunto.findByInvercion", query = "SELECT d FROM Detalleasunto d WHERE d.invercion = :invercion"),
    @NamedQuery(name = "Detalleasunto.findByRecursos", query = "SELECT d FROM Detalleasunto d WHERE d.recursos = :recursos"),
    @NamedQuery(name = "Detalleasunto.findByTiempoNecesario", query = "SELECT d FROM Detalleasunto d WHERE d.tiempoNecesario = :tiempoNecesario")})
public class Detalleasunto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ganancia")
    private double ganancia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Tiempo")
    private String tiempo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Invercion")
    private double invercion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Recursos")
    private String recursos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "TiempoNecesario")
    private String tiempoNecesario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detalleasunto")
    private List<Asuntovotacion> asuntovotacionList;

    public Detalleasunto() {
    }

    public Detalleasunto(Integer id) {
        this.id = id;
    }

    public Detalleasunto(Integer id, double ganancia, String tiempo, double invercion, String recursos, String tiempoNecesario) {
        this.id = id;
        this.ganancia = ganancia;
        this.tiempo = tiempo;
        this.invercion = invercion;
        this.recursos = recursos;
        this.tiempoNecesario = tiempoNecesario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getGanancia() {
        return ganancia;
    }

    public void setGanancia(double ganancia) {
        this.ganancia = ganancia;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public double getInvercion() {
        return invercion;
    }

    public void setInvercion(double invercion) {
        this.invercion = invercion;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    public String getTiempoNecesario() {
        return tiempoNecesario;
    }

    public void setTiempoNecesario(String tiempoNecesario) {
        this.tiempoNecesario = tiempoNecesario;
    }

    @XmlTransient
    public List<Asuntovotacion> getAsuntovotacionList() {
        return asuntovotacionList;
    }

    public void setAsuntovotacionList(List<Asuntovotacion> asuntovotacionList) {
        this.asuntovotacionList = asuntovotacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalleasunto)) {
            return false;
        }
        Detalleasunto other = (Detalleasunto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Detalleasunto[ id=" + id + " ]";
    }
    
}
