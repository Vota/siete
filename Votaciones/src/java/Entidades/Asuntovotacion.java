/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Marii
 */
@Entity
@Table(name = "asuntovotacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asuntovotacion.findAll", query = "SELECT a FROM Asuntovotacion a"),
    @NamedQuery(name = "Asuntovotacion.findById", query = "SELECT a FROM Asuntovotacion a WHERE a.asuntovotacionPK.id = :id"),
    @NamedQuery(name = "Asuntovotacion.findByAsunto", query = "SELECT a FROM Asuntovotacion a WHERE a.asunto = :asunto"),
    @NamedQuery(name = "Asuntovotacion.findByFechadeRegistro", query = "SELECT a FROM Asuntovotacion a WHERE a.fechadeRegistro = :fechadeRegistro"),
    @NamedQuery(name = "Asuntovotacion.findByVotacionesaFavor", query = "SELECT a FROM Asuntovotacion a WHERE a.votacionesaFavor = :votacionesaFavor"),
    @NamedQuery(name = "Asuntovotacion.findByVotacionesenContra", query = "SELECT a FROM Asuntovotacion a WHERE a.votacionesenContra = :votacionesenContra"),
    @NamedQuery(name = "Asuntovotacion.findByStatus", query = "SELECT a FROM Asuntovotacion a WHERE a.status = :status"),
    @NamedQuery(name = "Asuntovotacion.findByIdDetalleAsunto", query = "SELECT a FROM Asuntovotacion a WHERE a.asuntovotacionPK.idDetalleAsunto = :idDetalleAsunto"),
    @NamedQuery(name = "Asuntovotacion.findByPropuso", query = "SELECT a FROM Asuntovotacion a WHERE a.propuso = :propuso"),
    @NamedQuery(name = "Asuntovotacion.findByPuesto", query = "SELECT a FROM Asuntovotacion a WHERE a.puesto = :puesto")})
public class Asuntovotacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AsuntovotacionPK asuntovotacionPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Asunto")
    private String asunto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechadeRegistro")
    @Temporal(TemporalType.DATE)
    private Date fechadeRegistro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VotacionesaFavor")
    private int votacionesaFavor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VotacionesenContra")
    private int votacionesenContra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Status")
    private boolean status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Propuso")
    private String propuso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Puesto")
    private String puesto;
    @JoinColumn(name = "idDetalleAsunto", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Detalleasunto detalleasunto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAsunto")
    private List<Votaciones> votacionesList;

    public Asuntovotacion() {
    }

    public Asuntovotacion(AsuntovotacionPK asuntovotacionPK) {
        this.asuntovotacionPK = asuntovotacionPK;
    }

    public Asuntovotacion(AsuntovotacionPK asuntovotacionPK, String asunto, Date fechadeRegistro, int votacionesaFavor, int votacionesenContra, boolean status, String propuso, String puesto) {
        this.asuntovotacionPK = asuntovotacionPK;
        this.asunto = asunto;
        this.fechadeRegistro = fechadeRegistro;
        this.votacionesaFavor = votacionesaFavor;
        this.votacionesenContra = votacionesenContra;
        this.status = status;
        this.propuso = propuso;
        this.puesto = puesto;
    }

    public Asuntovotacion(int id, int idDetalleAsunto) {
        this.asuntovotacionPK = new AsuntovotacionPK(id, idDetalleAsunto);
    }

    public AsuntovotacionPK getAsuntovotacionPK() {
        return asuntovotacionPK;
    }

    public void setAsuntovotacionPK(AsuntovotacionPK asuntovotacionPK) {
        this.asuntovotacionPK = asuntovotacionPK;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public Date getFechadeRegistro() {
        return fechadeRegistro;
    }

    public void setFechadeRegistro(Date fechadeRegistro) {
        this.fechadeRegistro = fechadeRegistro;
    }

    public int getVotacionesaFavor() {
        return votacionesaFavor;
    }

    public void setVotacionesaFavor(int votacionesaFavor) {
        this.votacionesaFavor = votacionesaFavor;
    }

    public int getVotacionesenContra() {
        return votacionesenContra;
    }

    public void setVotacionesenContra(int votacionesenContra) {
        this.votacionesenContra = votacionesenContra;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getPropuso() {
        return propuso;
    }

    public void setPropuso(String propuso) {
        this.propuso = propuso;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public Detalleasunto getDetalleasunto() {
        return detalleasunto;
    }

    public void setDetalleasunto(Detalleasunto detalleasunto) {
        this.detalleasunto = detalleasunto;
    }

    @XmlTransient
    public List<Votaciones> getVotacionesList() {
        return votacionesList;
    }

    public void setVotacionesList(List<Votaciones> votacionesList) {
        this.votacionesList = votacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asuntovotacionPK != null ? asuntovotacionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asuntovotacion)) {
            return false;
        }
        Asuntovotacion other = (Asuntovotacion) object;
        if ((this.asuntovotacionPK == null && other.asuntovotacionPK != null) || (this.asuntovotacionPK != null && !this.asuntovotacionPK.equals(other.asuntovotacionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Asuntovotacion[ asuntovotacionPK=" + asuntovotacionPK + " ]";
    }
    
}
