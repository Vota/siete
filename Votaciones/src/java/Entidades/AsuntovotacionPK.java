/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Marii
 */
@Embeddable
public class AsuntovotacionPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idDetalleAsunto")
    private int idDetalleAsunto;

    public AsuntovotacionPK() {
    }

    public AsuntovotacionPK(int id, int idDetalleAsunto) {
        this.id = id;
        this.idDetalleAsunto = idDetalleAsunto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDetalleAsunto() {
        return idDetalleAsunto;
    }

    public void setIdDetalleAsunto(int idDetalleAsunto) {
        this.idDetalleAsunto = idDetalleAsunto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) idDetalleAsunto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsuntovotacionPK)) {
            return false;
        }
        AsuntovotacionPK other = (AsuntovotacionPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.idDetalleAsunto != other.idDetalleAsunto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.AsuntovotacionPK[ id=" + id + ", idDetalleAsunto=" + idDetalleAsunto + " ]";
    }
    
}
