/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Entidades.Asuntovotacion;
import Entidades.Detalleasunto;
import Entidades.Usuarios;
import Entidades.Votaciones;
import Sesiones.AsuntovotacionFacade;
import Sesiones.DetalleasuntoFacade;
import Sesiones.UsuariosFacade;
import Sesiones.VotacionesFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.constraints.*;
        
/**
 *
 * @author 
 */

public class Login implements Serializable{
    //--------Inyección de dependencias---------
    @EJB
    private UsuariosFacade usuarioconn;
    @EJB
    private AsuntovotacionFacade asuntoconn;
    @EJB
    private DetalleasuntoFacade detallesconn;
    @EJB
    private VotacionesFacade votacionesconn;
    //------------------------------------------
    
    //------------Clases de Entidad-------------
    private Usuarios usuario;
    private Asuntovotacion asunto;
    private Detalleasunto detalle;
    private Votaciones votacion;
    //------------------------------------------
    
    //------------Listas de Entidad-------------
    private List<Usuarios> usuarios;
    private List<Asuntovotacion> asuntos;
    private List<Detalleasunto> detalles;
    private List<Votaciones> votaciones;
    //------------------------------------------
    
    //------Variables de Login y Busqueda-------
    @NotNull
    @Size(min = 6,message = "Login demaciado Corto")
    private String login;
    @NotNull
    @Size(min = 8,message = "El password no debe ser menor a 8 caracters")
    private String password;
    private Date Fecha;
    private String nomAsunto;
    private int numAsunto;
    //------------------------------------------
    
    /**
     * Creates a new instance of Login
     */
    public Login() {
    }
    
    //--------Setters y Getters Facades----------
    public UsuariosFacade getUsuarioconn() {
        return usuarioconn;
    }
    public void setUsuarioconn(UsuariosFacade usuarioconn) {
        this.usuarioconn = usuarioconn;
    }
    public AsuntovotacionFacade getAsuntoconn() {
        return asuntoconn;
    }
    public void setAsuntoconn(AsuntovotacionFacade asuntoconn) {
        this.asuntoconn = asuntoconn;
    }
    public DetalleasuntoFacade getDetallesconn() {
        return detallesconn;
    }
    public void setDetallesconn(DetalleasuntoFacade detallesconn) {
        this.detallesconn = detallesconn;
    }
    public VotacionesFacade getVotacionesconn() {
        return votacionesconn;
    }
    public void setVotacionesconn(VotacionesFacade votacionesconn) {
        this.votacionesconn = votacionesconn;
    }
    //-------------------------------------------
    
    //-------Setters y Getters Entidades---------
    public Usuarios getUsuario() {
        return usuario;
    }
    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }
    public Asuntovotacion getAsunto() {
        return asunto;
    }
    public void setAsunto(Asuntovotacion asunto) {
        this.asunto = asunto;
    }
    public Detalleasunto getDetalle() {
        return detalle;
    }
    public void setDetalle(Detalleasunto detalle) {
        this.detalle = detalle;
    }
    public Votaciones getVotacion() {
        return votacion;
    }
    public void setVotacion(Votaciones votacion) {
        this.votacion = votacion;
    }
    //-------------------------------------------
    
    //---------Setters y Getters Listas----------
    public List<Usuarios> getUsuarios() {
        usuarios=(List<Usuarios>)usuarioconn.findAll();
        return usuarios;
    }
    public void setUsuarios(List<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }
    public List<Asuntovotacion> getAsuntos() {
        asuntos=(List<Asuntovotacion>)asuntoconn.findAll();
        return asuntos;
    }
    public void setAsuntos(List<Asuntovotacion> asuntos) {
        this.asuntos = asuntos;
    }
    public List<Detalleasunto> getDetalles() {
        detalles=(List<Detalleasunto>)detallesconn.findAll();
        return detalles;
    }
    public void setDetalles(List<Detalleasunto> detalles) {
        this.detalles = detalles;
    }
    public List<Votaciones> getVotaciones() {
        votaciones=(List<Votaciones>)votacionesconn.findAll();
        return votaciones;
    }
    public void setVotaciones(List<Votaciones> votaciones) {
        this.votaciones = votaciones;
    }
    //-------------------------------------------
    
    //-------Setters y Getters Variablea---------
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Date getFecha() {
        return Fecha;
    }
    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }
    public String getNomAsunto() {
        return nomAsunto;
    }
    public void setNomAsunto(String nomAsunto) {
        this.nomAsunto = nomAsunto;
    }
    public int getNumAsunto() {
        return numAsunto;
    }
    public void setNumAsunto(int numAsunto) {
        this.numAsunto = numAsunto;
    }
    //-------------------------------------------
    
    //----------Metodos de Aplicacion------------   
    public String submit() {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "mesaDirectiva";
    }
    //-------------------------------------------
}