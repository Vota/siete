package Entidades;

import Entidades.AsuntovotacionPK;
import Entidades.Detalleasunto;
import Entidades.Votaciones;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-05-26T05:10:47")
@StaticMetamodel(Asuntovotacion.class)
public class Asuntovotacion_ { 

    public static volatile SingularAttribute<Asuntovotacion, Date> fechadeRegistro;
    public static volatile SingularAttribute<Asuntovotacion, String> puesto;
    public static volatile SingularAttribute<Asuntovotacion, AsuntovotacionPK> asuntovotacionPK;
    public static volatile SingularAttribute<Asuntovotacion, Integer> votacionesaFavor;
    public static volatile SingularAttribute<Asuntovotacion, Detalleasunto> detalleasunto;
    public static volatile SingularAttribute<Asuntovotacion, String> asunto;
    public static volatile SingularAttribute<Asuntovotacion, Integer> votacionesenContra;
    public static volatile ListAttribute<Asuntovotacion, Votaciones> votacionesList;
    public static volatile SingularAttribute<Asuntovotacion, String> propuso;
    public static volatile SingularAttribute<Asuntovotacion, Boolean> status;

}