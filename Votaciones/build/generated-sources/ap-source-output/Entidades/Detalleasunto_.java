package Entidades;

import Entidades.Asuntovotacion;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-05-26T05:10:47")
@StaticMetamodel(Detalleasunto.class)
public class Detalleasunto_ { 

    public static volatile SingularAttribute<Detalleasunto, String> tiempoNecesario;
    public static volatile SingularAttribute<Detalleasunto, Double> invercion;
    public static volatile SingularAttribute<Detalleasunto, String> tiempo;
    public static volatile ListAttribute<Detalleasunto, Asuntovotacion> asuntovotacionList;
    public static volatile SingularAttribute<Detalleasunto, Integer> id;
    public static volatile SingularAttribute<Detalleasunto, String> recursos;
    public static volatile SingularAttribute<Detalleasunto, Double> ganancia;

}